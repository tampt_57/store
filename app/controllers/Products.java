package controllers;


        import com.avaje.ebean.Ebean;
        import com.google.common.io.Files;
        import models.Product;
        import models.Tag;
        import play.data.Form;
        import play.mvc.Controller;
        import play.mvc.Result;
        import com.avaje.ebean.Page;

        import views.html.products.list;
        import views.html.products.details;
        import views.html.products.productDetails;
        import static play.mvc.Http.MultipartFormData;

        import java.io.File;
        import java.io.IOException;

        import java.util.ArrayList;
        import java.util.List;



/**
 * Created by Phan Tam on 4/8/2015.
 */
public class Products extends Controller {

    public static Result list( Integer page){
        Page<Product> products = Product.find(page);
        return ok(list.render(products));
    }

    private static final Form<Product> productForm = Form.form(Product.class);
    public static Result newProduct(){
        return ok(details.render(productForm));
    }

    public static Result details(Product product){
        if (product == null){
            return notFound(String.format("Product %s not exist", product.ean));
        }
        return ok(productDetails.render(product));
    }

    public static Result save(){
        Form<Product> boundForm = productForm.bindFromRequest();
        if (boundForm.hasErrors()){
            flash("error", "Please correct thr form below");
            return badRequest(details.render(boundForm));
        }
        Product product = boundForm.get();
        MultipartFormData body = request().body().asMultipartFormData();
        MultipartFormData.FilePart part = body.getFile("picture");
        if (part!=null){
            File picture = part.getFile();
            try {
                product.picture = Files.toByteArray(picture);
            }catch (IOException e){
                return internalServerError("Error reading file upload");
            }
        }
        List<Tag> tags = new ArrayList<Tag>();
        for (Tag tag : product.tags){
            if (tag.id != null){
                tags.add(Tag.findById(tag.id));
            }
        }
        product.tags = tags;
        if (product.id == null){
            Ebean.save(product);
        }else {
            if (product.ean != null){
                flash("Error, Ean exits !!!", "Please enter again Ean");
            }
            Ebean.update(product);
        }
        return redirect(routes.Products.list(0));
    }

    public static Result delete(String ean) {
        Product product = Product.findByEan(ean);
        if(product == null) {
            return notFound(String.format("Product %s does not exists.", ean));
        }
        for (Tag tag: product.tags) {
            tag.products.remove(product);
            tag.save();
        }
        for (Tag tag : product.tags) {
            tag.products.remove(product);
            tag.update();
        }

        product.delete();
        return redirect(routes.Products.list(0));
    }

    public static Result edit(Product product){
        if (product == null){
            return notFound(String.format("Product %s not exist", product.ean));
        }
        Form<Product> filledForm = productForm.fill(product);
        return ok(details.render(filledForm));
    }
    public static Result picture(String ean){
        final Product product = Product.findByEan(ean);
        if(product==null) return notFound();
        return ok(product.picture);
    }

}
