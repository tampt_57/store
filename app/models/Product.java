package models;

import com.avaje.ebean.Page;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import play.mvc.PathBindable;
import play.data.validation.Constraints;
import scala.util.Either;

/**
 * Created by Phan Tam on 3/14/2015.
 */
@Entity
public class Product extends Model implements PathBindable<Product> {
    @Constraints.Required
    public String ean;
    @Constraints.Required
    public String name;
    public String description;
    public byte[] picture;
    public double price;
    public int remainProduct;
    @Id
    public Long id;


    @ManyToMany
    public List<Tag> tags;
    @Formats.DateTime(pattern = "dd-MM-yyyy")
    public Date date;
    private static List<Product> products;
    public static Finder<Long, Product> find = new Finder<Long, Product>(Long.class, Product.class);

    public Product() {
    }

    public Product(String ean, String name, String description, double price, int remainProduct) {
        this.description = description;
        this.ean = ean;
        this.name = name;
        this.price = price;
        this.remainProduct = remainProduct;
    }

    public String toString() {
        return String.format("%s - %s", ean, name);
    }

    public static List<Product> findAll() {
        return find.all();
    }

    public static Product findByEan(String ean) {

        return find.where().eq("ean", ean).findUnique();
    }

    public static List<Product> findByName(String term) {
        final List<Product> results = new ArrayList<Product>();
        for (Product candidate : products) {
            if (candidate.name.toLowerCase().contains(term.toLowerCase())) {
                results.add(candidate);
            }
        }
        return results;
    }



    @Override
    public Product bind(String key, String value) {
        return findByEan(value);
    }

    @Override
    public String unbind(String key) {
        return ean;
    }

    @Override
    public String javascriptUnbind() {
        return ean;
    }

    public static Page<Product> find(int page){
        return find.where()
                .orderBy("id asc")
                .findPagingList(2)
                .setFetchAhead(false)
                .getPage(page);
    }

}
